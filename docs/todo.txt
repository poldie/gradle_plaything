Done (to some extent)
---------------------
Eclipse
git
egit
gradle
sonarlint
guice
junit


Todo
----
actions (jenkins alternative)
aspect j weaver
batch (xml, jsr whatever)
buildship
cloud
cloud-dev box
cloud-docker
cloud-jenkins
cygwin
docker
effective java 3rd edition
erm/crud
git extension
gradle plugin for eclipse
guava
guice alternatives/compatibility
hibernate/jpa
Java
java string utils
java-check youtube/ctu for relevant stuff
java-modern stuff (java 11? 14? generics etc)
java-type erasure
jenkins
log4j
maven
mingw32
mocking frameworks
node.js
oracle
RESTful stuff
sonarcube/other linters
spring/spring boot
tdd
test switching between oracle/something else
