package com.poldie.practice1;

public class Penguin extends Bird {

	private static final int LIFESPAN = 18;
	private static final boolean CAN_QUACK = false;
	private static final boolean CAN_FLY = false;
	private static final boolean CAN_SWIM = true;

	public Penguin(String name) {
		super(LIFESPAN, name);
		super.setCanQuack(CAN_QUACK);
		super.setCanSwim(CAN_SWIM);
		super.setCanFly(CAN_FLY);
	}

}
