package com.poldie.practice1;

public abstract class Mammal extends Animal {

	private boolean canBark;
	private boolean canMiaow;

	public boolean isCanBark() {
		return canBark;
	}

	public void setCanBark(boolean canBark) {
		this.canBark = canBark;
	}

	public boolean isCanMiaow() {
		return canMiaow;
	}

	public void setCanMiaow(boolean canMiaow) {
		this.canMiaow = canMiaow;
	}

	public Mammal(int lifespan, String name) {
		super(lifespan, name);
		 
	}

}
