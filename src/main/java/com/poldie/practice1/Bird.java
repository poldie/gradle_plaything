package com.poldie.practice1;

public abstract class Bird extends Animal {

	private boolean canQuack;
	private boolean canFly;
	private boolean canSwim;

	public boolean isCanQuack() {
		return canQuack;
	}

	public void setCanQuack(boolean canQuack) {
		this.canQuack = canQuack;
	}

	public boolean isCanFly() {
		return canFly;
	}

	public void setCanFly(boolean canFly) {
		this.canFly = canFly;
	}

	public boolean isCanSwim() {
		return canSwim;
	}

	public void setCanSwim(boolean canSwim) {
		this.canSwim = canSwim;
	}

	public Bird(int lifespan, String name) {
		super(lifespan, name);

	}
}
