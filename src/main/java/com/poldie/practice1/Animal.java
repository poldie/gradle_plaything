package com.poldie.practice1;

public abstract class Animal {

	final private int lifespan;
	final private String name;

	public String getName() {
		return name;
	}

	public int getLifespan() {
		return lifespan;
	}

	public Animal(int lifespan, String name) {
		this.lifespan = lifespan;
		this.name = name;
	}
}
