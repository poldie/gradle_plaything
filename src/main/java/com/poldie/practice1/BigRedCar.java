package com.poldie.practice1;

public class BigRedCar extends RedCar {

	public String textbox="big";

	public BigRedCar(String model) {
		super(model);
	}

	public int howBig() {
		return 456;
	}
	
	@Override
	public int howRed() {
		return 200;
	}

	@Override
	public String getMpg() {		
		return "BigRedCar: 300mpg";
	}

	public void showTextbox() {

		System.out.println(String.format("BRC - showtextbox"));

		String txtbrc = textbox;
		System.out.println(String.format("BRC: %s", txtbrc));
	
		String txtrc = ((RedCar)this).textbox;
		System.out.println(String.format("RC: %s", txtrc));

		String txtrc2 = super.textbox;
		System.out.println(String.format("RC again: %s", txtrc2));

		String txtc = ((Car)this).textbox;			
		System.out.println(String.format("C: %s", txtc));

		Object cj = this;
		if (cj instanceof Object) System.out.println("Object1");
		if (cj instanceof BigRedCar) System.out.println("BigRedCar1");
		if (cj instanceof RedCar) System.out.println("RedCar1");
		if (cj instanceof Car) System.out.println("Car1");
		Car justACar = this;
		if (justACar instanceof Object) System.out.println("Object2");
		if (justACar instanceof BigRedCar) System.out.println("BigRedCar2");
		if (justACar instanceof RedCar) System.out.println("RedCar2");
		if (justACar instanceof Car) System.out.println("Car2");
		String regularCarText = justACar.textbox;
		System.out.println(String.format("Justacar: %s", regularCarText));
	
		if (this instanceof Walkable) System.out.println("Walkable");
		
		///////////////////
		
		int hwBrc = howRed();					// 200
		int hwrc = super.howRed();				// 123
		RedCar rc = this;
		int hwrc2 = rc.howRed();				// 200
		int hwrc3 = ((RedCar)this).howRed();	// 200
		int hwrc4 = ((Car)this).howRed();		// 200
			
		
	}
	
	
}
