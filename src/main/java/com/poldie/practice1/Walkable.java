package com.poldie.practice1;

public interface Walkable {
	public static final float MAX_SPEED = 2.5f; 
	public static float MIN_SPEED = 1.0f; 			// this is final even if not declared as such
	public void walk();
}
