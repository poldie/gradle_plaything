package com.poldie.practice1;

public class Cat extends Mammal {

	private static final int LIFESPAN = 13;
	private static final boolean CAN_MIAOW = true;
	private static final boolean CAN_BARK = false;

	public Cat(String name) {
		super(LIFESPAN, name);
		super.setCanBark(CAN_BARK);
		super.setCanMiaow(CAN_MIAOW);
	}

}
