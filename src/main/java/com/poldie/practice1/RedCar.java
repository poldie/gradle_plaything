package com.poldie.practice1;

public class RedCar extends Car {

	public String textbox = "red";

	public RedCar(String model) {
		super(model);
	}

	@Override
	public int howRed() {
		return 123;
	}

	@Override
	public String getMpg() {
		return "RedCar: 200mpg";
	}

	
	public void showTextbox() {

		System.out.println(String.format("RC - showtextbox"));

		String txtrc = textbox;
		System.out.println(String.format("RC: %s", txtrc));
		
		String txtc = ((Car)this).textbox;			
		System.out.println(String.format("C: %s", txtc));
		
	}

	
}
