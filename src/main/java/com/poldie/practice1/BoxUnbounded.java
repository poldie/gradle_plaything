package com.poldie.practice1;

public class BoxUnbounded<T> {

	protected T value;

	public void box(T t) {
		value = t;
	}

	public T unbox() {
		T t = value;
		value = null;
		return t;
	}

}