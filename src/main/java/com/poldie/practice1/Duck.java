package com.poldie.practice1;

public class Duck extends Bird {

	private static final int LIFESPAN = 12;
	private static final boolean CAN_QUACK = true;
	private static final boolean CAN_FLY = true;
	private static final boolean CAN_SWIM = false;

	public Duck(String name) {
		super(LIFESPAN, name);
		super.setCanQuack(CAN_QUACK);
		super.setCanSwim(CAN_SWIM);
		super.setCanFly(CAN_FLY);
	}

}
