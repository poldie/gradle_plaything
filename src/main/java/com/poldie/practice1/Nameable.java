package com.poldie.practice1;

public interface Nameable {

	public void setName(String name);
	public String getName();
	public static String returnHello() {
		return "Hello";
	}
	
	default public int return12() {
		return 12;
	}

	public long returnTwiceWhatIsPassed(int n);
	public long returnTwiceWhatIsPassed(short n);
	public long returnTwiceWhatIsPassed(long n);
	public long returnTwiceWhatIsPassed(char n);
	


}
