package com.poldie.practice1;

public class Car {

	protected final String Model;
	public String textbox = "car";

	public Car(String model) {
		Model = model;
	}

	public String getModel() {
		return Model;
	}

	public int howRed() {
		return 0;
	}
	public String getMpg() {		
		return "Car: 100mpg";
	}

	public void showTextbox() {

		System.out.println(String.format("C - showtextbox"));
		
		String txtc = textbox;			
		System.out.println(String.format("C: %s", txtc));
		
	}

	
}
