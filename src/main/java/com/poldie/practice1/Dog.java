package com.poldie.practice1;

public class Dog extends Mammal {

	private static final int LIFESPAN = 12;
	private static final boolean CAN_MIAOW = false;
	private static final boolean CAN_BARK = true;

	public Dog(String name) {
		super(LIFESPAN, name);
		super.setCanBark(CAN_BARK);
		super.setCanMiaow(CAN_MIAOW);
	}

}
