package com.poldie.calculator;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Provide methods to perform calculations
 */
public class Calculator {

	private static Logger logger = LogManager.getLogger(Calculator.class);

	protected int calculatorId = 12345;
		
	public Calculator() {
		logger.debug("in Calculator ctor");
	}

	public Calculator(int i) {
		// call another constructor
		this();
		System.out.println("Using the int constructor, eh?");	
	}

	public Calculator(float f) {
		// call another constructor
		this((int)f);
		System.out.println("Using the float constructor, eh?");	
	}
	
	
	
	
	/**
	 * @param param1 first int to add
	 * @param param2 second int to add
	 * @return sum of param1 and param2
	 */
	public int add(int param1, int param2) {
		logger.debug("add (int): L{} {}, {}", Thread.currentThread().getStackTrace()[1].getLineNumber(), param2);
		return param1 + param2;
	}

	/**
	 * @param param1 first float to add
	 * @param param2 second float to add
	 * @return sum of param1 and param2
	 */
	public float add(float param1, float param2) {
		logger.debug("add (float): {}, {}", param1, param2);
		return param1 + param2;
	}

	/**
	 * @param param1 first int to subtract
	 * @param param2 second int to subtract
	 * @return param1 - param2
	 */
	public int subtract(int param1, int param2) {
		logger.debug("subtract (int): {}, {}", param1, param2);
		return param1 - param2;
	}

	/**
	 * @param param1 first float to subtract
	 * @param param2 second float to subtract
	 * @return param1 - param2
	 */
	public float subtract(float param1, float param2) {
		logger.debug("subtract (float): {}, {}", param1, param2);
		return param1 - param2;
	}

	/**
	 * @param param1 first int to multiply
	 * @param param2 second int to multiply
	 * @return param1 * param2
	 */
	public int multiply(int param1, int param2) {
		logger.debug("multiply(int): {}, {}", param1, param2);
		return param1 * param2;
	}

	/**
	 * @param param1 first float to multiply
	 * @param param2 second float to multiply
	 * @return param1 * param2
	 */
	public float multiply(float param1, float param2) {
		logger.debug("multiply(float): {}, {}", param1, param2);
		return param1 * param2;
	}

	/**
	 * @param param1 first float to divide
	 * @param param2 second float to divide
	 * @return param1 / param2
	 */
	public int divide(int param1, int param2) {
		logger.debug("divide(int): {}, {}", param1, param2);
		return param1 / param2;
	}

	/**
	 * @param param1 first float to divide
	 * @param param2 second float to divide
	 * @return param1 / param2
	 */
	public float divide(float param1, float param2) {
		logger.debug("divide(float): {}, {}", param1, param2);
		logger.printf(Level.DEBUG, "divide(float): %.2f, %.2f", param1, param2);
		return param1 / param2;
	}

}
