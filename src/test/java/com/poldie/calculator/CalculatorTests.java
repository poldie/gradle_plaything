package com.poldie.calculator;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CalculatorTests {
	private static Logger logger = LogManager.getLogger(CalculatorTests.class);
	
	@Test
	public void addIntegers() {
		Calculator calc = new Calculator();
		logger.fatal("Adding integers");
		assertEquals(2, calc.add(1, 1), "1 + 1 is 2");
	}

	@Test
	public void subtractIntegers() {
		Calculator calc = new Calculator();
		assertEquals(3, calc.subtract(10, 7), "10 - 7 is 3");
	}

}
